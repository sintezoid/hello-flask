FROM python:3.9-alpine
MAINTAINER Mikhail Zhuchkov <mzhuchkovu@oxydmins.ru>

COPY . /app
WORKDIR /app
RUN pip install -r /app/requirements.txt
EXPOSE 5000

ENTRYPOINT ["python3", "/app/app.py"]
