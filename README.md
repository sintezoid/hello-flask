# hello flask
Простое приложение на flask для деплоя в kubernetes

# В директории deploy лежат чистые yaml
Для примера

# helm
В директории helm лежат темплейты helm чарта

Ниже вариант деплоя helm локально
```
REF=$(git symbolic-ref -q --short HEAD | sed 's/\//-/');
Получаем имя ветки

COMMIT=$(git rev-parse --short HEAD);
Получаем hash коммита

NAME="user1-"$REF;
Генерируем имя чарта
helm --namespace vlab upgrade --install $NAME ./helm \
--set timestamp=$(date +%s) \
--set imageTag=$REF \
--set imageCommit=$COMMIT \
--set url="$NAME.vlab.oxdydmins.ru"
```
Внимание! upgrade --install нужно для совмещения в одном шаге установки если чарта нет и апдейта, если он есть.