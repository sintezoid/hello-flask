from flask import Flask
import os
app = Flask(__name__)

@app.route('/')
def hello_world():
    param = os.getenv("k8sparam")
    if param == None:
        message = "Flask в контейнере Docker"
    else:
        message = "Flask в контейнере" + " " + param
    return message

if __name__ == '__main__':
  app.run(debug=True,host='0.0.0.0')
